//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<flutter_true_call/FlutterTrueCallPlugin.h>)
#import <flutter_true_call/FlutterTrueCallPlugin.h>
#else
@import flutter_true_call;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FlutterTrueCallPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterTrueCallPlugin"]];
}

@end
