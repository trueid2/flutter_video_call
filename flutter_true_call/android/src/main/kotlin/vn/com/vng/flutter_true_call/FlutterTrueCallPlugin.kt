package vn.com.vng.flutter_true_call

import android.app.Activity
import androidx.annotation.NonNull;
import io.flutter.Log
import io.flutter.app.FlutterApplication
import java.util.*
import android.content.Context
import android.content.res.Configuration
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import vng.trueid.call.TrueCall
import vng.trueid.call.TrueCallListener
import vng.trueid.call.TrueCallResult
import vng.trueid.call.model.ConfigInfo

class FlutterTrueCallPlugin: FlutterPlugin, MethodCallHandler, ActivityAware {
  private lateinit var channel : MethodChannel
  private lateinit var activity: Activity

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_true_call")
    channel.setMethodCallHandler(this);
  }

  override fun onDetachedFromActivity() {
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    this.activity = binding.activity;
  }

  override fun onDetachedFromActivityForConfigChanges() {
  }

  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "flutter_true_call")
      channel.setMethodCallHandler(FlutterTrueCallPlugin())
    }
  }

  @Suppress("UNCHECKED_CAST")
  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    val arguments: Map<String, Any> = call.arguments as Map<String, Any>

    when (call.method!!) {
      "configure" -> {
        if (!arguments.containsKey("config")) {
          result.success("TrueID Error: config not found.")
          return
        }
        val config: Map<String, Any> = arguments["config"] as Map<String, Any>

        if ( !config.containsKey("authDomain") || !config.containsKey("authDomainPath") || !config.containsKey("appId") || !config.containsKey("appSecret")) {
          result.success("TrueID Error: config is incorrect.")
        }

        val info: ConfigInfo =
          ConfigInfo( config["authDomain"] as String, config["authDomainPath"] as String, config["appId"] as String, config["appSecret"] as String,config["serverBaseURL"] as String)


        TrueCall.configure(activity, info)

//        setupLanguage(arguments)

        result.success("")
      }

      "setLanguage" -> {
        setupLanguage(arguments)
        result.success("")
      }

      "startCall" -> {
        TrueCall.startCall(activity,
          arguments["queueId"] as String?,
          arguments["requestType"] as String?, arguments["metaData"] as String?,object :TrueCallResult{
          override fun onResult(code: Int ){
            result.success(code)
          }
        })

      }

      "initCall" -> {
        TrueCall.initCall(activity, object : TrueCallListener {
          override fun  connection(isConnected: Boolean) {
            try {
              activity.runOnUiThread {
                Log.d("Trueid","return : "+isConnected)
                result.success(isConnected)
              }
            }catch (e:Exception){
              e.printStackTrace()
            }
          }
        })
      }

      else -> {
        result.notImplemented()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

    private fun setupLanguage(arguments: Map<String, Any>) {
        if (arguments.containsKey("language")) {
          TrueCall.setLanguage(activity, arguments["language"] as String)
            // Override application language with the selected locale
            val locale = Locale(arguments["language"] as String)
            val config: Configuration = activity.getBaseContext().getResources().getConfiguration()
            config.setLocale(locale)
            // Update current configuration
            activity.getBaseContext().getResources().updateConfiguration(config, activity.getBaseContext().getResources().getDisplayMetrics())
        }
    }
  }
