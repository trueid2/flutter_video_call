package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import vn.com.vng.flutter_true_call.FlutterTrueCallPlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    FlutterTrueIdPlugin.registerWith(registry.registrarFor("vn.com.vng.flutter_true_call.FlutterTrueCallPlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
