import Flutter
import UIKit
import TrueIDVideoSDK

public class SwiftFlutterTrueCallPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_true_call", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterTrueCallPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arguments = call.arguments as! [String: Any]
        
        switch call.method {
        case "configure":
            guard let map = arguments["config"] as? [String: Any] else {
                result("TrueID Error: config not found.")
                return
            }
            
            guard let authDomain = map["authDomain"] as? String,
                let authDomainPath = map["authDomainPath"] as? String,
                let appId = map["appId"] as? String,
                let appSecret = map["appSecret"] as? String,
                 let serverBaseURL = map["serverBaseURL"] as? String
            else {
                result("TrueID Error: config is incorrect.")
                return
            }
            
            let info: ConfigInfo

            info = ConfigInfo( authDomain: authDomain, authDomainPath: authDomainPath, appId: appId, appSecret: appSecret, serverBaseURL: serverBaseURL )
            TrueID.configure(configInfo: info)
            // if let zoomLicenseKey = map["zoomLicenseKey"] as? String,
            //     let zoomServerBaseURL = map["zoomServerBaseURL"] as? String,
            //     let zoomPublicKey = map ["zoomPublicKey"] as? String,
            //     let zoomAuthURL = map ["zoomKeyURL"] as? String {
                
              
                
            //     self.setupLanguage(arguments: arguments)
            // }

            break
            
        case "setLanguage":
            self.setupLanguage(arguments: arguments)
            
            // return without error message
            result("")
            break
        case "initCall": 
            result(true)
            break
        case "startCall":
            TrueID.start(data: arguments["metaData"] as! String, requestType: arguments["requestType"] as! String, queueId: arguments["queueId"] as! String)  { (r: Int) in
                // convert card info to map
                result(r)
            }
            break
            
        case "requestId":
//            let id = TrueID.requestId()
//            result(id)
            break
            
        default:
            result(FlutterMethodNotImplemented)
            break
        }
    }
    
    fileprivate func setupLanguage(arguments: [String: Any]) {
        if let lang = arguments["language"] as? String {
            TrueID.setLanguage(language: lang)
        }
    }
}
