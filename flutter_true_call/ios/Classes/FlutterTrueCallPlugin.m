#import "FlutterTrueCallPlugin.h"
#if __has_include(<flutter_true_call/flutter_true_call-Swift.h>)
#import <flutter_true_call/flutter_true_call-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_true_call-Swift.h"
#endif

@implementation FlutterTrueCallPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterTrueCallPlugin registerWithRegistrar:registrar];
}
@end
