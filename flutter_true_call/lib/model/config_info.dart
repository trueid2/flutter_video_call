import 'package:flutter/cupertino.dart';

class ConfigInfo {

  String authDomain;
  String authDomainPath;

  String appId;
  String appSecret;

  String serverBaseURL;

  ConfigInfo({
    @required this.authDomain,
    @required this.authDomainPath,
    @required this.appId,
    @required this.appSecret,
    @required this.serverBaseURL,
  });

  Map toMap() {
    Map res = {
      "authDomain": authDomain,
      "authDomainPath": authDomainPath,
      "appId": appId,
      "appSecret": appSecret,
      "serverBaseURL": serverBaseURL,
    };
    return res;
  }
}