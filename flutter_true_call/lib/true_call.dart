import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter_true_call/model/config_info.dart';

class TrueCall {
  static const MethodChannel _channel =
      const MethodChannel('flutter_true_call');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> configure(ConfigInfo config,
      [String languageCode]) async {
    Map args = {"config": config.toMap()};

    if (languageCode != null) {
      args["language"] = languageCode;
    }
    _channel.invokeMethod("configure", args);
    return "";
  }

  static void setLanguage(String languageCode) async {
    _channel.invokeMethod("setLanguage", {"language": languageCode});
  }

  static Future<int> startCall(String queueId,String requestType,String metaData) async {
    final int result = await  _channel.invokeMethod("startCall", {"queueId": queueId,"requestType": requestType,"metaData": metaData});
    return result;
  }

  static Future<bool> initCall() async {
    final bool result = await _channel.invokeMethod("initCall", {});
    return result;
  }
}
