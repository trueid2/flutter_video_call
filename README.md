# trueiddemo

TrueID Demo - verify face selfie 
## 1.5.6

# RESOURCE
    - flutter_true_id: source flutter sử dụng TrueID
    - trueiddemo: source chạy demo TrueID

## Run demo

 * Open file main.dart
 * Cập nhật var config = ConfigInfo(
      domain: "",
      domainPath: "/ekyc/v1.0",
      authDomain: "",
      authDomainPath: "/v1/oauth",
      appId: "",
      appSecret: "",
    ); do TrueID cung cấp 

    TrueId.configure(config, "vi"); // vi là language code, language là optional
    TrueId.newRequest("accessToken", "requestId","endpoint"); 
    // "endpoint" là url gọi verify hình ảnh đối với Trueid là "https://api.trueid.ai/ekyc/v1.0/selfie/verify"
    - set language
    TrueId.setLanguage("vi") // vi là language code

    vui lòng xem file CHANGELOG trong thư mục "flutter_true_id"

    