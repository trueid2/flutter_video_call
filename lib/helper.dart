import 'package:flutter/cupertino.dart';

class Helper {
  static Images images = Images();

  static Future<T> pushPage<T>(BuildContext context, Widget page, {bool fullscreenDialog = false}) {
    return Navigator.of(context).push<T>(CupertinoPageRoute(builder: (context) => page, fullscreenDialog: fullscreenDialog));
  }

  static void popPage<T>(BuildContext context) {
    Navigator.of(context).pop();
  }
}

class Images {
  final String bgHome = 'assets/images/bg_home.png';
  final String bgResult = 'assets/images/bg_result.png';

  final String icFaceVerification = 'assets/images/ic_face_verification.png';
  final String icIDValidation = 'assets/images/ic_id_validation.png';
  final String icLiveDetection = 'assets/images/ic_live_detection.png';
  final String icVerified = 'assets/images/ic_verified.png';
  final String icFailed = 'assets/images/ic_failed.png';

  final String logoTrueID = 'assets/images/logo_true_id.png';
}

/// === LOADING === ///
_IndicatorRoute _indicator;

void showLoading(BuildContext context) {
  if (_indicator == null) {
    _indicator = _IndicatorRoute();

//    Future.delayed(Duration.zero, () => Navigator.of(context, rootNavigator: true).push(_indicator));
    Navigator.of(context, rootNavigator: true).push(_indicator);
  }
}

void hideLoading(BuildContext context) {
  if (_indicator != null) {
    Navigator.of(context, rootNavigator: true).pop(_indicator);
    _indicator = null;
  }
}

class _IndicatorRoute<T> extends ModalRoute {
  _IndicatorRoute({bool barrierDismissible = true, String barrierLabel, Color barrierColor = const Color(0x80000000), RouteSettings settings})
      : assert(barrierDismissible != null),
        _barrierDismissible = barrierDismissible,
        _barrierLabel = barrierLabel,
        _barrierColor = barrierColor,
        super(settings: settings) {
    Future.delayed(loadingTimeout, () {
      isClosable = true;
    });
  }

  final Duration loadingTimeout = Duration(seconds: 30);
  bool isClosable = false;

  @override
  bool get barrierDismissible => _barrierDismissible;
  final bool _barrierDismissible;

  @override
  String get barrierLabel => _barrierLabel;
  final String _barrierLabel;

  @override
  Color get barrierColor => _barrierColor;
  final Color _barrierColor;

  @override
  bool get opaque => false;

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => Duration.zero;

  @override
  Future<RoutePopDisposition> willPop() {
    if (!isClosable) return Future.value(RoutePopDisposition.doNotPop);
    return super.willPop();
  }

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return GestureDetector(
      onTap: () {
        if (isClosable) hideLoading(context);
      },
      child: Semantics(
        child: CupertinoActivityIndicator(
          radius: 20,
          animating: true,
        ),
        scopesRoute: true,
        explicitChildNodes: true,
      ),
    );
  }
}