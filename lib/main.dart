import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_true_call/model/config_info.dart';
import 'package:flutter_true_call/true_call.dart';
import 'package:trueiddemo/helper.dart';
import 'package:trueiddemo/result_page.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(onGenerateRoute: (RouteSettings settings) {
    if (settings.name == '/') {
      return new MaterialPageRoute<Null>(
        settings: settings,
        builder: (_) => new MyApp(),
        maintainState: false,
      );
    }
    return null;
  }));
}

class MyApp extends StatefulWidget {
  MyAppState createState() => new MyAppState();
}

class MyAppState extends State<MyApp> with TickerProviderStateMixin {
  AnimationController _controller;
  bool _isCanMakeCall = false;

  _onLayoutDone(_) {
    //do your stuff
    print("_onLayoutDone");

  }

  Future<void> initCallData() async {
    bool value = await TrueCall.initCall();
    _showToast(context,value);
  }

  @override
  void initState() {
    print("initState was called");
    _controller = new AnimationController(vsync: this)
      ..repeat(min: 0.0, max: 1.0, period: const Duration(seconds: 1))
      ..addListener(() {});

    var config = ConfigInfo(
      authDomain: "https://stag.trueid.ai",
      authDomainPath: "/v1/oauth",
      appId: "9a7e572cace946107c3e806c086e0218",
      appSecret: "AXjOZWvX3pNE7oJYHS150GI56296wh38QQ+fZTv6HSA=",
      serverBaseURL: "wss://videocall-stag.trueid.vn/ws2", //pro
    );
    TrueCall.configure(config);
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    super.initState();
    initCallData();
  }

  @override
  void dispose() {
    print("dispose was called");
    _controller.dispose();
    super.dispose();
  }

  void _showToast(BuildContext context, bool isConnect) async {
    setState(() {
      _isCanMakeCall = isConnect;
    });
    final scaffold = ScaffoldMessenger.of(context);
    print("isConnect"+isConnect.toString());
    if (isConnect) {
      scaffold.showSnackBar(
        SnackBar(
          content: const Text('You Can Make a Call'),
          // action: SnackBarAction(label: 'Call Now', onPressed: scaffold.hideCurrentSnackBar),
        ),
      );
    } else {
      scaffold.showSnackBar(
        SnackBar(
          content: const Text('You Can not Make Call'),
          // action: SnackBarAction(label: 'Call Now', onPressed: scaffold.hideCurrentSnackBar),
        ),
      );
    }
  }

  void makeCall() {
    // if (_isCanMakeCall) {
      TrueCall.startCall("","","ewogICJDdXN0b21lcklkIjogIndlZndlODEyOCIsCiAgIlVzZXJuYW1lIjogIlRy4bqnbiB2xINuIGjDuW5nIiwKICAiUGhvbmUiOiAiMDkxMjM0NTY3OCIsCiAgIkVtYWlsIjogImh1bmd0dkBnbWFpbC5jb20iLAogICJQYWNrZWQiOiAiMTAwMDAwMDAwIiwKICAiUmVxUGFja2VkIjogIjUwMDAwMDAwMCIKfQ==");
    // } else {}
  }

  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('home screen')),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            makeCall();
          },
          child: new Text("Make Call"),
        ),
      ),
      // floatingActionButton: new FloatingActionButton(
      //   child: new Icon(Icons.remove_red_eye),
      //   onPressed: () {
      //     setState(() {
      //       _isCanMakeCall = !_isCanMakeCall;
      //     });
      //   },
      // ),
    );
  }
}

