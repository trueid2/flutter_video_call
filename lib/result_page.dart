import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trueiddemo/helper.dart';

class ResultPage extends StatelessWidget {

  
  @override
  Widget build(BuildContext context) {
    TextStyle titleStyle = TextStyle(fontSize: 14, color: Color(0xFF858585));
    TextStyle valueStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Color(0xFF3D3D3D));

    return CupertinoPageScaffold(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Helper.images.bgResult),
            fit: BoxFit.cover,
          ),
        ),
        child: Flex(
          direction: Axis.vertical,
          children: <Widget>[
            Expanded(
              flex: 53,
              child: Container(
                // safe padding top
                padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16), bottomRight: Radius.circular(16)),
                  color: Colors.white,
                ),
                // child: Flex(
                //   direction: Axis.vertical,
                //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: <Widget>[
                //     // avatar
                //     Container(),
                //     ClipRRect(
                //       borderRadius: BorderRadius.all(Radius.circular(50)),
                //       child: Image.memory(this.cardInfo.person.selfie, width: 100, height: 100, fit: BoxFit.cover),
                //     ),
                //
                //     // name
                //     Text(this.cardInfo.person.givenPlace, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Color(0xFF0D5FDB))),
                //
                //     // information date of birth, gender, id number
                //     Flex(
                //       direction: Axis.horizontal,
                //       children: <Widget>[
                //         Expanded(
                //           flex: 36,
                //           child: Column(
                //             children: <Widget>[
                //               Text("Date of birth", style: titleStyle),
                //               Text(this.cardInfo.person.dob, style: valueStyle),
                //             ],
                //           ),
                //         ),
                //         Expanded(
                //           flex: 28,
                //           child: Column(
                //             children: <Widget>[
                //               Text("Gender", style: titleStyle),
                //               Text(this.cardInfo.person.doi, style: valueStyle),
                //             ],
                //           ),
                //         ),
                //         Expanded(
                //           flex: 36,
                //           child: Column(
                //             children: <Widget>[
                //               Text("ID Number", style: titleStyle),
                //               Text(this.cardInfo.person.idNumber, style: valueStyle),
                //             ],
                //           ),
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
              ),
            ),
            Expanded(
              flex: 47,
              child: Flex(
                direction: Axis.vertical,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Personal identity have passed the test", style: TextStyle(fontSize: 16, color: Colors.white)),

                  // status
                  Flex(
                    direction: Axis.horizontal,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // children: <Widget>[
                    //   _statusCell(Helper.images.icIDValidation, "ID\nValidation", this.cardInfo.getRecord(DetectionType.idVerification)),
                    //   _statusCell(Helper.images.icFaceVerification, "Face\nVerification", this.cardInfo.getRecord(DetectionType.faceComparision)),
                    //   _statusCell(Helper.images.icLiveDetection, "Liveness\nDetection", this.cardInfo.getRecord(DetectionType.livenessDetection)),
                    // ],
                  ),

                  GestureDetector(
                    onTap: () => Helper.popPage(context),
                    child: Container(
                      alignment: Alignment.center,
                      width: 240,
                      height: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Color(0xFF3DCDFB),
                      ),
                      child: Text("Start another demo", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)),
                    ),
                  ),
                  Container(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _statusCell(String icon, String title, CheckingRecord record) {
  //   return Container(
  //     width: 90,
  //     height: 90,
  //     child: Stack(
  //       children: <Widget>[
  //         Positioned(
  //           left: 25,
  //           right: 25,
  //           top: 10,
  //           child: Image.asset(icon, width: 40, height: 40, fit: BoxFit.contain),
  //         ),
  //
  //         Positioned(
  //           right: 18,
  //           top: 2,
  //           child: Image.asset(record.status ? Helper.images.icVerified : Helper.images.icFailed, width: 18, height: 18, fit: BoxFit.contain),
  //         ),
  //
  //         Positioned(
  //           left: 0,
  //           right: 0,
  //           bottom: 0,
  //           child: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.white)),
  //         )
  //       ],
  //     ),
  //   );
  // }
}