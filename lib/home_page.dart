import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_true_call/model/config_info.dart';
import 'package:flutter_true_call/true_call.dart';
import 'package:trueiddemo/helper.dart';
import 'package:trueiddemo/result_page.dart';
import 'package:flutter/foundation.dart';

class HomePage extends StatelessWidget {

  HomePage() {
    var config = ConfigInfo(
      authDomain:  "https://stag.trueid.ai",
      authDomainPath: "/v1/oauth",
      appId: "d9302ff7455758886605b80bf1b4be40",//UAT
      appSecret: "p6ZTtJAM2gGBFUWCd0yulS++puvUcvCuxpYHaRBqMow=",//UAT
      serverBaseURL:  "wss://videocall-stag.trueid.vn/ws2",//pro
    );
    TrueCall.configure(config);
    // TrueCall.setLanguage("vi");
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    double buttonPadding = (screenSize.width - 220) / 2;

    return CupertinoPageScaffold(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Helper.images.bgHome), fit: BoxFit.cover),
        ),
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              // logo
              Positioned(
                top: screenSize.height * 0.2,
                left: 0,
                right: 0,
                child: Image.asset(Helper.images.logoTrueID,
                    width: 207, height: 112),
              ),

              // button
              Positioned(
                left: buttonPadding,
                right: buttonPadding,
                top: screenSize.height * 0.5,
                child: GestureDetector(
                  onTap: () async {
                    // call TrueID.start
                    // showLoading(context);
                    TrueCall.setLanguage("vi");
                    // hideLoading(context);


                  },
                  child: Container(
                    height: 44,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.white,
                    ),
                    child: Text("START CALL",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: Colors.black)),
                  ),
                ),
              ),

              // copyright
              Positioned(
                left: 0,
                right: 0,
                bottom: 20,
                child: Text("Copyright © 2021. Datalab",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12, color: Colors.white)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
